package cz.cvut.eshop.shop;

import org.junit.Before;
import org.junit.Test;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DiscountedItemTest {

    DiscountedItem item;

    @Before
    public void createDiscountedItem () {

        item = new DiscountedItem(1, "dreveny kaktus",
                10 , "hracky", 10,
                new Date( 2008,12, 4,10,10),
                new Date( 2008,12, 4,10, 10));


    }

    @Test
    public void testSetDiscountFrom_correctInput(){
        item.setDiscountFrom("10.10.2005");
        Date d  = new Date (2005 - 1900, 10 - 1, 10);
        assertEquals(d, item.getDiscountFrom());
    }

    @Test
    public void testSetDiscountFrom_wrongInput(){
        item.setDiscountFrom("abcd");
    }


    @Test
    public void testDiscountItemCopy(){
        DiscountedItem copy = item.copy();
        assertEquals(copy, item);
    }







}
